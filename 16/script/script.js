const loginImgSrc = "./img/01.svg";

const loginMarkup = `
  <div class="login-page">
    <div class="login-img">
      <img src="${loginImgSrc}" alt="Login icon">
    </div>
    <div class="form">
      <h1>Login</h1>
      <form class="input-form">
        <label for="email">Email</label>
        <input type="email" id="email">
        <label for="password">Password</label>
        <input type="password" id="password">
        <p class="message">Not a user? <a href="#" id="register-link">Register now</a></p>
        <button>Login</button>            
      </form>
    </div>    
  </div>
`;

document.body.innerHTML = loginMarkup;

const registerLink = document.getElementById("register-link");
registerLink.addEventListener("click", () => {
    window.location.href = "./registration.html";
});

//console.log(localStorage)

const form = document.querySelector('form');
const emailInput = document.querySelector('#email');
const passwordInput = document.querySelector('#password');


form.addEventListener('submit', (event) => {
    event.preventDefault();

    const storedEmail = localStorage.getItem('email');
    const storedPassword = localStorage.getItem('password');
    
    if (emailInput.value === storedEmail && passwordInput.value === storedPassword) {
        window.location.href = './registration.html';
    } else {
        alert('Invalid email or password');
    }
});
