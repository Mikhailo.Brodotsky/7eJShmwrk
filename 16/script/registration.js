const regImgSrc = "./img/02.svg",
      namePat = /^[a-zA-Zа-яА-Я]{2,30}(([',. -][a-zA-Zа-яА-Я ])?[a-zA-Zа-яА-Я]{1,30})*$/,
      dobPat = /^\d{4}-\d{2}-\d{2}$/,
      emailPat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      mTelPat = /^\+\d{12}$/,
      passwordPat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;

const registrationMarkup = `
  <div class="registration-page">
    <div class="registration-img">
      <img src="${regImgSrc}" alt="registration icon">
    </div>
    <div class="form">
      <h1 class="h-reg">Registration form</h1>
      <form class="input-form input-reg">
        <label for="name">Name</label>
        <input type="text" id="name">
        <label for="dob">Date Of Birth</label>
        <input type="date" id="dob">
        <label for="secondname">Father’s/Mother’s Name</label>
        <input type="text" id="secondname">
        <label for="email">Email</label>
        <input type="email" id="email">
        <label for="mTel">Mobile No.</label>
        <input type="tel" id="mTel">
        <label for="password">Password</label>
        <input type="password" id="password">
        <label for="rePas">Re-enter Password</label>
        <input type="password" id="rePas">
        <label for="tel">home Number</label>
        <input type="tel" id="tel">                        
        <button class="sub-btn">sunmit</button>            
      </form>
    </div>
  </div>
`;

document.body.innerHTML = registrationMarkup;

const validate = (pattern, value) => pattern.test(value);

const validateField = function(field, pattern) {
    const isValid = validate(pattern, field.value);
    field.classList.toggle('red', !isValid);
    field.classList.toggle('green', isValid);
};

const nameField = document.getElementById('name'),
      dobField = document.getElementById('dob'),
      secondNameField = document.getElementById('secondname'),
      emailField = document.getElementById('email'),
      mTelField = document.getElementById('mTel'),
      passwordField = document.getElementById('password'),
      rePasField = document.getElementById('rePas'),
      telField = document.getElementById('tel'),
      submitButton = document.querySelector('.sub-btn');

nameField.addEventListener('change', function() {
    validateField(nameField, namePat);
});

dobField.addEventListener('change', function() {
    validateField(dobField, dobPat);
});

secondNameField.addEventListener('change', function() {
    validateField(secondNameField, namePat);
});

emailField.addEventListener('change', function() {
    validateField(emailField, emailPat);
});

mTelField.addEventListener('change', function() {
    validateField(mTelField, mTelPat);
});

passwordField.addEventListener('change', function() {
    validateField(passwordField, passwordPat);
});

rePasField.addEventListener('change', function() {
    const isValid = rePasField.value === passwordField.value;
    rePasField.classList.toggle('red', !isValid);
    rePasField.classList.toggle('green', isValid);
});

telField.addEventListener('change', function() {
    validateField(telField, mTelPat);
});


submitButton.addEventListener('click', function(event) {
    event.preventDefault();  
    const isValid = (
        validate(namePat, nameField.value) && 
        validate(dobPat, dobField.value) &&
        validate(namePat, secondNameField.value) &&
        validate(emailPat, emailField.value) &&
        validate(mTelPat, mTelField.value) &&
        validate(passwordPat, passwordField.value) &&
        (rePasField.value === passwordField.value) &&
        validate(mTelPat, telField.value)
    );
  
    if (isValid) {
        localStorage.setItem('name', nameField.value);
        localStorage.setItem('dob', dobField.value);
        localStorage.setItem('secondname', secondNameField.value);
        localStorage.setItem('email', emailField.value);
        localStorage.setItem('password', passwordField.value);
        localStorage.setItem('mTel', mTelField.value);
        localStorage.setItem('tel', telField.value);
  
         window.location.href = 'index.html';
        } else {
            alert('Please fill in all fields correctly.');
    }
});