import {clickInputSize, clickSauceAdd, clickToppingAdd} from "./functionEvent.js"
import {runningOut, validateField} from "./functions.js"

document.getElementById("pizza")
    .addEventListener("click", clickInputSize);

document.querySelectorAll(".topping")
.forEach((div)=>{
    div.addEventListener("dragstart", clickToppingAdd)
})

document.querySelectorAll(".sauce")
.forEach((div)=>{
    div.addEventListener("dragstart", clickSauceAdd)
})

export const pizzaSelectUser = {
   size : "",
   topping : [],
   sauce : "",
   price : 0
}


document.querySelector(".ingridients")
    .addEventListener('dragstart', (event) => {

    event.target.addEventListener('dragstart', function (evt) {
        evt.dataTransfer.effectAllowed = "move";
        evt.dataTransfer.setData("Text", this.id);
    }, false);

    const dropZone = document.getElementById("target");

    dropZone.addEventListener("dragover", function (evt) {
        if (evt.preventDefault) evt.preventDefault();
        return false;
    }, false);
    
    
    dropZone.addEventListener("drop", function (evt) { 

        const priceOut = document.getElementById('price')
        const sauceOut = document.getElementById('sauce')
        const toppingOut = document.getElementById('topping')

        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        const drop = event.target.cloneNode(true);
        this.appendChild(drop);

        const toppingToinerHtml = [] 
        for (let i = 0; i < pizzaSelectUser.topping.length; i++) {
            toppingToinerHtml.push(pizzaSelectUser.topping[i].productName);
            toppingOut.innerHTML = toppingToinerHtml.toString('')
        }
      
        if (event.target.closest('div').className === "sauce") {
            console.log(pizzaSelectUser)
            sauceOut.innerHTML = pizzaSelectUser.sauce.name
            toppingOut.innerHTML = toppingToinerHtml.toString('')
        }

        priceOut.innerHTML = pizzaSelectUser.price
        return false;
    }, false);
   
})

document.getElementById("banner").
    addEventListener("mouseover", runningOut);

const name = document.querySelector('[name="name"]'),
      namePat = /^[a-zA-Zа-яА-Я]{2,30}(([',. -][a-zA-Zа-яА-Я ])?[a-zA-Zа-яА-Я]{1,30})*$/,
      phone = document.querySelector('[name="phone"]'),
      phonePat = /^\+\d{12}$/,
      email = document.querySelector('[name="email"]'),
      emailPat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

name.onchange = () => validateField(name, namePat);
phone.onchange = () => validateField(phone, phonePat);
email.onchange = () => validateField(email, emailPat);
    