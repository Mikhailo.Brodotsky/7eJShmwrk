/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло
 повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число.
 Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло
 повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число.
 Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const calculator = {
    displayValue: '0',
    firstOperand: null,
    waitingForSecondOperand: false,
    mem: null,
    waitingForMem: false,
    operator: null,
};

const mOnDisplay = document.createElement("p");
mOnDisplay.classList = "memletter";
document.querySelector('.display').append(mOnDisplay);


const updateDisplay = () => {
    const display = document.querySelector('.display > input');
    display.value = calculator.displayValue;
}
updateDisplay()

const resetCalculator = () => {
    calculator.displayValue = '0'
    calculator.firstOperand = null;
    calculator.waitingForSecondOperand = false;
    calculator.operator = null;
}

const keys = document.querySelector('.keys');

keys.addEventListener('click', (event) => {
    const { target } = event;
   
    if(!target.matches('input')) {
        return;
    }
    
    if (target.classList.contains('decimal')) {
        inputDecimal(target.value);
        updateDisplay();
        return;
    }

    if (target.classList.contains('clearance')) {
        resetCalculator();
        updateDisplay();
        return;
    }

    if (target.classList.contains('operator')) {
        haldleOperator(target.value);
        updateDisplay();
        return;
    }

    if (target.classList.contains('gray')) {
        switch (target.value) {
            case 'm+':                
                mOnDisplay.textContent = target.value.charAt(0);                            
                calculator.mem += +calculator.displayValue; 
                calculator.waitingForSecondOperand = false
                break;
            case 'm-':
                mOnDisplay.textContent = target.value.charAt(0);  
                calculator.mem += -calculator.displayValue;
                calculator.waitingForSecondOperand = false
                break;
            case 'mrc':
                if (calculator.waitingForMem === false) {
                    calculator.displayValue = calculator.mem;
                    calculator.waitingForMem = true;
                    calculator.waitingForSecondOperand = false
                    document.querySelector('.display > input').value = calculator.mem;
                } else {
                    mOnDisplay.textContent = "" 
                    calculator.mem = null;
                    calculator.displayValue = '0'
                    calculator.waitingForMem = false;
                    updateDisplay(); 
                }
                break;              
        }
        return;
    }

    inputDigit(target.value);
    updateDisplay();  

});

const inputDigit = (digit) => {
    const { displayValue, waitingForSecondOperand } = calculator;
    if (waitingForSecondOperand === true) {
        calculator.displayValue = digit;
        calculator.waitingForSecondOperand = false;
    } else {
        calculator.displayValue = displayValue === '0' ? digit : displayValue + digit;
    }
}

const inputDecimal = (dot) => {
    if (calculator.waitingForSecondOperand === true) {
        calculator.displayValue = '0.';
        calculator.waitingForSecondOperand = false;
        return;
    }
    if (!calculator.displayValue.includes(dot)) {
        calculator.displayValue += dot;
    }
};

const haldleOperator = (nextOperator) => {
    const { firstOperand, displayValue, operator } = calculator;
    const inputValue = parseFloat(displayValue);

    if (operator && calculator.waitingForSecondOperand) {
        calculator.operator = nextOperator;
        return;
    }
    if(firstOperand == null && !isNaN(inputValue)) {
        calculator.firstOperand = inputValue;
    } else if (operator) {
        const result = calculate (firstOperand, inputValue, operator)

        calculator.displayValue = `${parseFloat(result.toFixed(3))}`;
        calculator.firstOperand = result;
    }

    calculator.waitingForSecondOperand = true;
    calculator.operator = nextOperator;
    document.querySelector('.orange').disabled = false;   
}

const calculate = (firstOperand, secondOperand, operator) => {
    if(operator === '+') {
        return firstOperand + secondOperand;
    } else if (operator === '-') {
        return firstOperand - secondOperand;
    } else if (operator === '*') {
        return firstOperand * secondOperand;
    } else if (operator === '/') {
        return firstOperand / secondOperand;
    } 
    return secondOperand;  
}




