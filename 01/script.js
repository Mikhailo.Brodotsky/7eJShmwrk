document.write('<header style="box-sizing: border-box; display: flex; align-content: center; justify-content: center; width: 800px; height: 200px; border: 1px solid orange; "><div style="display: flex; align-items: center; justify-content: center;">header 800*200</div></header>');
document.write('<nav style="box-sizing: border-box; display: inline-flex; align-content: center; justify-content: center; width: 100px; height: 400px; border: 1px solid orange; "> <div style="display: flex; align-items: center; justify-content: center;">nav 100*400</div></nav>');
document.write('<section style="box-sizing: border-box; display: inline-flex; align-content: center; justify-content: center; width: 700px; height: 400px; border: 1px solid orange; "> <div class="inject" style="display: flex; align-items: center; justify-content: center;"></div></section>');
document.write('<footer style="box-sizing: border-box; display: flex; align-content: center; justify-content: center; width: 800px; height: 200px; border: 1px solid orange; "><div style="display: flex; align-items: center; justify-content: center;">footer 800*200</div></footer>');

var name = prompt("Type your name");
var surname = prompt("Type your surname");
var age = prompt("Type your age");
var firstInput = prompt("Type first number");
var secondInput = prompt("Type second number");
var thirdInput = prompt("Type third number");
var arithmetic = (parseFloat(firstInput) + parseFloat(secondInput) + parseFloat(thirdInput))/3;

var injectData = `Your name is ${name}, Your surname is ${surname}, Your age is ${age}. Arithmetic ${arithmetic}`;

var inject = document.querySelector('.inject');
var title = document.createElement('div');
inject.appendChild(title);
title.className = 'title';
title.innerHTML = injectData;

var x = 6;
var y = 14;
var z = 4;
var result = x += y - x++ * z;
document.write('x=6; y=14; z=4' + '<br/>');
document.write('x += y - x++ * z = ?' + '<br/>');
document.write('x += y - (x++ => 6) * z' + ' prefix increment has priority' + '<br/>');
document.write('x += y - (6 * 4)' + ' multiplication has priority' + '<br/>');
document.write('x += (14 - 24)' + ' subtraction has priority' + '<br/>');
document.write('6 += -10' + ' addition assignment is the last operation' + '<br/>');
document.write('-4' + '<br/>');
document.write('Summary: ' + result + '<br/>' + '<br/>');

var x = 6;
var y = 14;
var z = 4;
var result = z = --x - y * 5;
document.write('x=6; y=14; z=4' + '<br/>');
document.write('z = --x - y * 5' + '<br/>');
document.write('z = (--x => 5) - y * 5' + ' prefix decrement has priority' + '<br/>');
document.write('z = 5 - (14 * 5)' + ' multiplication has priority' + '<br/>');
document.write('z = (5 - 70)' + ' subtraction has priority' + '<br/>');
document.write('z = -65' + ' assignment is the last operation' + '<br/>');
document.write('-65' + '<br/>');
document.write('Summary: ' + result + '<br/>' + '<br/>');

var x = 6;
var y = 14;
var z = 4;
var result = y /= x + 5 % z;
document.write('x=6; y=14; z=4' + '<br/>');
document.write('y /= x + 5 % z' + '<br/>');
document.write('y /= x + (5 % 4)' + ' remainder has priority' + '<br/>');
document.write('y /= (6 + 1)' + ' addition has priority' + '<br/>');
document.write('14 /= 7' + ' division assignment is the last operation' + '<br/>');
document.write('2' + '<br/>');
document.write('Summary: ' + result + '<br/>' + '<br/>');

var x = 6;
var y = 14;
var z = 4;
var result = z - x++ + y * 5;
document.write('x=6; y=14; z=4' + '<br/>');
document.write('z - x++ + y * 5' + '<br/>');
document.write('z - (x++ => 6) + y * 5' + ' postfix Increment has priority' + '<br/>');
document.write('z - 6 + (14 * 5)' + ' multiplication has priority' + '<br/>');
document.write('4 - 6 + 70' + ' subtraction and additionis with same priority level from left to right' + '<br/>');
document.write('68' + '<br/>');
document.write('Summary: ' + result + '<br/>' + '<br/>');

var x = 6;
var y = 14;
var z = 4;
var result = x = y - x++ * z;
document.write('x=6; y=14; z=4' + '<br/>');
document.write('x = y - x++ * z' + '<br/>');
document.write('x = y - (x++ => 6) * z' + ' postfix Increment has priority' + '<br/>');
document.write('x = y - (6 * 4)' + ' multiplication has priority' + '<br/>');
document.write('x = (10 - 24)' + ' subtraction has priority' + '<br/>');
document.write('x = -10' + ' assignment is the last operation' + '<br/>');
document.write('-10' + '<br/>');
document.write('Summary: ' + result + '<br/>' + '<br/>');


