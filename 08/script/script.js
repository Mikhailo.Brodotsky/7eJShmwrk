/*
Виконуємо ось цю роботу : Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло".
Дана кнопка повинна бути єдиним контентом у тілі HTML документа,
решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript.
При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола.
При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору.
При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво.
*/

document.getElementById("start").onclick = () => {
    const input = document.createElement("input");
    const button = document.createElement("button");
    const playground = document.createElement("section");
    
    playground.classList.add("playground");
    
    input.value="75";
    input.type="number";
    button.innerHTML = "Намалювати";
    
    document.body.appendChild(input);
    document.body.appendChild(button)     
    document.body.appendChild(playground);
    document.getElementById("start").remove();
    playground.setAttribute('style', `display: grid; grid-template-columns: auto auto auto auto auto auto auto auto auto auto; justify-content: center;`);
    
    const playgroundProcessing = document.querySelector('.playground'); 
    
    document.querySelector('button').addEventListener('click',()=> {
        playgroundProcessing.innerHTML = '';
//            for(let i = 0; i < 10; i++) {
                const diameter = document.querySelector('[type="number"]').value;
                for(let j = 0; j < 100; j++) {
                    playgroundProcessing.innerHTML += `<div style="
                    display:inline-block;
                    border-radius:50%;
                    border:1px solid;
                    width:${diameter}px;
                    height:${diameter}px;
                    background:hsl(${Math.floor(Math.random()*360)},70%,30%);
                    "></div>`;                                 
                }
   //         playgroundProcessing.innerHTML += "<br/>";
  //      };

    const playgroundArea = document.querySelectorAll('section > div');
            
    playgroundArea.forEach(circle => circle.addEventListener('click', playgroundAreaClearance));
          
    function playgroundAreaClearance() {
        const remove = this;
        remove.remove();
        }
    });
}