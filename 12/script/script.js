/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test" focus blur input

2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів.
Скільки символів має бути в інпуті, зазначається в атрибуті data-length.
Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.


4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`.
Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

function createNewElement (elemetName, elemetAttribute, elemetAttributeMeasume, elemetType, elemetPlaceholder, elemetCssClass) {
    const
        element = document.createElement(elemetName),
        attr = document.createAttribute(elemetAttribute);
    attr.value = elemetAttributeMeasume;
    element.setAttributeNode(attr);
    element.type = elemetType;    
    element.placeholder = elemetPlaceholder;
    element.classList.add(elemetCssClass);
    return element;
}

function addElement (elementCount) {    
    for (let i = 0; i < elementCount; i++) {
        let dataCount = Math.floor(Math.random() * 10) + 1
        const form = createNewElement('input', 'data-length', dataCount, 'text', 'name', 'input');
        document.querySelector('body').append(form);         
    }
}

addElement(5);

// task#1. 
const inputs = [...document.getElementsByClassName('input')];
const testPar = document.createElement('p');
testPar.id = 'test';
inputs[0].before(testPar);

inputs.forEach(element => {
    element.onblur = function() {
        document.getElementById('test').innerText = element.value;         
        };
});



// task#2. 
inputs.forEach(element => {
    element.onblur = function() {
        if (element.value.length == element.getAttribute('data-length')) {
            element.className = '';
            element.classList.add('green');
        } else {
            element.className = '';
            element.classList.toggle('red');
        }
        document.getElementById('test').innerText = element.value; 
    };   
});

// task#4. 
window.onload = function () {
    const
        wrapper = document.createElement('section'),
        divError = document.createElement('div'),
        input = createNewElement('input', 'data-length', 100, 'number', 'Price', 'input');
    document.querySelector('body').append(wrapper);
    
    wrapper.append(input);
    wrapper.append(divError);

    input.onfocus = function() {
        this.classList = '';
        this.classList.add('green');
    };

    input.onchange = function() {
        const
            spanWrapper = document.createElement('div'),
            inputSpan = document.createElement('span'),
            inputButton = document.createElement('button');
        document.querySelector('section > input').before(spanWrapper);
        if (this.value < 0) {
            this.classList = '';
            this.classList.add('red');
            divError.innerHTML = 'Please enter correct price';
        } else {
            divError.innerHTML = '';
            this.classList = '';
            this.classList.add('green');
            spanWrapper.append(inputButton);
            inputButton.before(inputSpan);
            inputButton.innerText = 'X';
            inputSpan.innerText = `some text ... ${input.value}`
            };

            const inputButtons = [...document.querySelectorAll("button")];
            
            inputButtons.forEach(button => {
                button.addEventListener('click', () => {
                    button.closest('div').remove();
                });
            });
    };

}
