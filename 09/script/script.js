function createTag(tagName, tagClass) {
    const tag = document.createElement(tagName);
    tag.innerText = tagName;    
    tag.classList.add(tagClass);
    return tag;
}

document.body.classList.add('body');
document.body.setAttribute('contenteditable', true);
document.body.innerText = document.body.innerText + "body";
const firstHeaderBlack = createTag('header', 'headerblack');
const firstNavBlue = createTag('nav', 'navblue');
const divWrapperFirst = document.createElement('div');
const firstSectionBlack = createTag('section', 'sectionblack');
const firstHeaderBlue =  createTag('header', 'headerblue');
const secondSectionBlack = createTag('section', 'sectionblack');
const firstArcticleBlue = createTag('article', 'articleblue');
const fisdtHeaderGreen = createTag('header', 'headergreen');
const fisdtPGreen = createTag('p', 'pgreen');
const divWrapperSecond = document.createElement('div');
const secondPGreen = createTag('p', 'pgreen');
const firstAsideGreen = createTag('aside', 'asidegreen');
const secondArcticleBlue = createTag('article', 'articleblue');
const secondHeaderGreen =  createTag('header', 'headergreen');
const thirdPGreen = createTag('p', 'pgreen');
const fourthPGreen = createTag('p', 'pgreen');
const firstFooterGreen = createTag('footer', 'footergreen');
const firstFooterBlue = createTag('footer', 'footerblue');
const secondHeaderBlue = createTag('header', 'headerblue');
const secondNavBlue = createTag('nav', 'navblue');
const firstFooterBlack = createTag('footer', 'footerblack');

document.body.appendChild(firstHeaderBlack);
firstHeaderBlack.appendChild(firstNavBlue);
document.body.appendChild(divWrapperFirst);
document.querySelector('div').classList.add('flexwrapper');
document.querySelector('div').appendChild(firstSectionBlack);
firstSectionBlack.appendChild(firstHeaderBlue);
firstSectionBlack.appendChild(firstArcticleBlue);
firstArcticleBlue.appendChild(fisdtHeaderGreen);
firstArcticleBlue.appendChild(fisdtPGreen);
firstArcticleBlue.appendChild(divWrapperSecond);
divWrapperSecond.appendChild(secondPGreen);
divWrapperSecond.appendChild(firstAsideGreen);
secondPGreen.classList.add('half');
firstAsideGreen.classList.add('half');
firstSectionBlack.appendChild(secondArcticleBlue);
secondArcticleBlue.appendChild(secondHeaderGreen);
secondArcticleBlue.appendChild(thirdPGreen);
secondArcticleBlue.appendChild(fourthPGreen);
secondArcticleBlue.appendChild(firstFooterGreen);
firstSectionBlack.appendChild(firstFooterBlue);
secondSectionBlack.appendChild(secondHeaderBlue);
secondSectionBlack.appendChild(secondNavBlue);
secondNavBlue.id='navgrowmax';
document.body.appendChild(firstFooterBlack);
firstSectionBlack.id='sectionblack1';
secondSectionBlack.id='sectionblack2';

divWrapperSecond.classList.add('flexwrapper');
document.querySelector('div').appendChild(secondSectionBlack);

metaDescription = `
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>hmwrJS09</title>
<link rel="stylesheet" href="./style/style.css">
`
document.head.innerHTML = metaDescription;

