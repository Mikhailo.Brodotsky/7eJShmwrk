function filterBy(dataIn, typeOfDataOut) {
    return dataIn.reduce(
       (accumulator, currentValue) => {
          if (typeof currentValue != typeOfDataOut) {
             accumulator.push(currentValue);
          } return accumulator}
          , []); // callback function for every element of dataIn[] + empty [] as initial value in reduce method for dataIn
}
const a = ['hello', 'world', 23, '23', null],
      b = 'string';

console.log(filterBy(a, b));

const words = ["программа","макака","прекрасный","оладушек"];
    // Выбираем случайное слово
const word = words[Math.floor(Math.random() * words.length)];
    // Создаем итоговый массив
const answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
    }
const remainingLetters = word.length;
    // Игровой цикл
while (remainingLetters > 0) {
    // Показываем состояние игры
alert(answerArray.join(" "));
    // Запрашиваем вариант ответа
const guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
if (guess === null) {
    // Выходим из игрового цикла
    break;
    } else if (guess.length !== 1) {
        alert("Пожалуйста, введите одиночную букву.");
    } else {
    // Обновляем состояние игры
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
    // Конец игрового цикла
}
    // Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert(`Отлично! Было загадано слово ${word}`);
