/*
  Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
  Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
  На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
  сторінок показувати нові коментарі. 
  з коментарів виводити : 
  "id": 1,
  "name"
  "email"
  "body"
  + час створення, одночасно забрати 500 елементів, потім обробка.
*/
const url = 'https://jsonplaceholder.typicode.com/comments?_page=1&_limit=500';
const pageSize = 10;
let currentPage = 1;
let comments = [];

const request = new XMLHttpRequest();
request.open("GET", url, true);
request.onload = function () {
  if (request.status === 200) {

    comments = JSON.parse(request.responseText);
    //console.log(comments)
    displayComments(currentPage);
    createPaginationButtons();
  }
};

function displayComments(page) {
 
  document.getElementById('comments').innerHTML = '';
  
  const startIndex = (page - 1) * pageSize;
  const endIndex = startIndex + pageSize;
  
  for (let i = startIndex; i < endIndex && i < comments.length; i++) {
    const comment = comments[i];
    const commentDiv = document.createElement('div');
    commentDiv.classList.add('comment');

    const idParagraph = document.createElement('p');
    idParagraph.textContent = 'ID: ' + comment.id;
    commentDiv.appendChild(idParagraph);

    const nameParagraph = document.createElement('p');
    nameParagraph.textContent = comment.name;
    nameParagraph.classList.add('comment-name');
    commentDiv.appendChild(nameParagraph);

    const emailParagraph = document.createElement('p');
    const emailLink = document.createElement('a');
    emailLink.textContent = comment.email;
    emailLink.setAttribute('href', 'mailto:' + comment.email);
    emailParagraph.textContent = 'Email: ';
    emailParagraph.appendChild(emailLink);
    commentDiv.appendChild(emailParagraph);

    const bodyParagraph = document.createElement('p');
    bodyParagraph.textContent = 'Comment: ' + comment.body;
    commentDiv.appendChild(bodyParagraph);
    
    const dateParagraph = document.createElement('p');
    const randomDate = new Date(Date.now() - Math.floor(Math.random() * 259200000)); // last 3 days
    dateParagraph.textContent = 'Posted on ' + randomDate.toLocaleString();
    commentDiv.appendChild(dateParagraph);

    document.getElementById('comments').appendChild(commentDiv);
  }
}

function createPaginationButtons() {
  const paginationContainer = document.createElement('div');
  paginationContainer.classList.add('pagination');
  for (let i = 1; i <= 50; i++) {
    const button = document.createElement('button');
    button.textContent = i;
    button.addEventListener('click', function () {
      currentPage = i;
      displayComments(currentPage);
    });
    paginationContainer.appendChild(button);
  }
  document.body.appendChild(paginationContainer);
}

request.send();




