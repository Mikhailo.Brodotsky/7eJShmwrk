/*
Виконати запит на https://swapi.dev/api/people (https://starwars-visualguide.com) отримати список героїв зіркових воєн.

Вивести кожного героя окремою карткою із зазначенням. картинки Імені, статевої приналежності, ріст, колір шкіри,
рік народження та планету на якій народився.

Створити кнопку зберегти на кожній картці. 
При натисканні кнопки записуйте інформацію у браузері
*/
const url = 'https://swapi.dev/api/people?page=5';
const container = document.querySelector('.container');

const request = new XMLHttpRequest();

request.open("GET", url);

request.responseType = 'json';

request.onload = function() {

  if (request.status === 200) {
    
    const data = request.response;
    //console.log(data);

    data.results.forEach(character => {
      const card = document.createElement('div');
      card.classList.add('card');

      const homeworldRequest = new XMLHttpRequest();
      homeworldRequest.open('GET', character.homeworld);
      homeworldRequest.responseType = 'json';
      homeworldRequest.onload = function() {
        if (homeworldRequest.status === 200) {
          const homeworldData = homeworldRequest.response;
          card.innerHTML = `
            <img src='https://starwars-visualguide.com/assets/img/characters/${getIdFromUrl(character.url)}.jpg' alt='${character.name}'>
            <h2>${character.name}</h2>
            <p class="card-info"><span>Sex:</span> ${character.gender}</p>
            <p class="card-info"><span>Height:</span> ${character.height} cm</p>
            <p class="card-info"><span>Skin color:</span> ${character.skin_color}</p>
            <p class="card-info"><span>Year of birth:</span> ${character.birth_year}</p>
            <p class="card-info"><span>Homeworld:</span> ${homeworldData.name}</p>
            <button class="save-btn">Save</button>
          `;
          container.appendChild(card);
          
          const saveBtn = card.querySelector('.save-btn');
          saveBtn.addEventListener('click', function() {
            
            const characterData = {
              name: character.name,
              sex: character.gender,
              height: character.height,
              skin_color: character.skin_color,
              birth_year: character.birth_year,
              homeworld: homeworldData.name,
              imageUrl: `https://starwars-visualguide.com/assets/img/characters/${getIdFromUrl(character.url)}.jpg`
            };
            localStorage.setItem(character.name, JSON.stringify(characterData)); // При натисканні кнопки записуйте інформацію у браузері -> так або просто створити об'єкт?
            //console.log(characterData)
            alert("Character saved!");
          });
        } else {
          console.log('Error: ' + homeworldRequest.status);
        }
      };
      homeworldRequest.send();
    });
  } else {
    console.log('Error: ' + request.status);
  }
};

function getIdFromUrl(url) {
  const parts = url.split('/');
  //console.log(parts[parts.length - 2])
  return parts[parts.length - 2];
}

request.send();



