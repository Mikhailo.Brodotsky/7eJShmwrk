/*
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним,
старт - зелений, скидання - сірий
* Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/


const stopwatch = document.querySelector('.container-stopwatch'),
    hoursOnDisplay = document.querySelector('.stopwatch-display > span'),
    minutesOnDisplay = document.querySelector('.stopwatch-display span:nth-child(2)'),
    secondsOnDisplay = document.querySelector('.stopwatch-display span:nth-child(3)'),
    btnStart = document.querySelector('.stopwatch-control > button'),
    btnStop = document.querySelector('.stopwatch-control button:nth-child(2)'),
    btnReset = document.querySelector('.stopwatch-control button:nth-child(3)');

let pass = 0,
    now = 0,
    hours = 0,
    minutes = 0,
    seconds = 0,
    onpause = true,
    intervalStopWatch;

btnStart.addEventListener('click', () => {
    
    stopwatch.classList.add('green');
    stopwatch.classList.remove('black','red','silver');

    if (onpause) {
        onpause = false;
        start = Date.now() - pass;
        intervalStopWatch = setInterval(update, 1000)
    }
})

btnStop.addEventListener('click', () => {
    
    stopwatch.classList.add('red');
    stopwatch.classList.remove('black','green','silver');
    
    if (!onpause) {
        onpause = true;
        pass = Date.now() - start;
        clearInterval(intervalStopWatch)
    }
})

btnReset.addEventListener('click', () => {
    
    stopwatch.classList.add('silver');
    stopwatch.classList.remove('black','green','red');
    
    onpause = true;
    
    clearInterval(intervalStopWatch);
    
    start = 0;
    pass = 0;
    now = 0;
    hours = 0;
    minutes = 0;
    seconds = 0;
    
    hoursOnDisplay.innerText = '00';
    minutesOnDisplay.innerText = '00';
    secondsOnDisplay.innerText = '00';
})

function update() {

    function doubleDigits(time){
        if (('0' + time).length > 2) {
            return time;
        } return '0' + time;
    }

    pass = Date.now() - start;

    seconds = Math.floor((pass / 1000) % 60);
    minutes = Math.floor((pass / 60000) % 60);
    hours = Math.floor((pass / 3600000 ) % 60);

    seconds = doubleDigits(seconds);
    minutes = doubleDigits(minutes);
    hours = doubleDigits(hours);

    hoursOnDisplay.innerText = hours;
    minutesOnDisplay.innerText = minutes;
    secondsOnDisplay.innerText = seconds;
}

/*
Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність
введення номера, якщо номер правильний зробіть зелене тло і використовуючи
document.location переведіть користувача на сторінку
https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg
якщо буде помилка, відобразіть її в діві до input.
*/


function validation(form) {
    let input = document.querySelector('input').value;
    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    return (pattern.test(input) && input.length == 13 );  
}

document.getElementById('form').addEventListener('submit', function(event){
    event.preventDefault()

    if (document.querySelector('.error')!==null) {
        document.querySelector('.error').remove()
    }

    if(validation(this)){
        document.querySelector('input').classList.add('greenbackground');
        document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
    } else{
        const erordiv = document.createElement("div")
        document.getElementById('form').before(erordiv);
        erordiv.innerHTML ='wrong input format, shoud be 000-000-00-00'
        erordiv.classList.add('error')
    }
}
)

/*
Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/


const images = [
    'https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
    'https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
    'https://naukatv.ru/upload/files/shutterstock_418733752.jpg',
    'https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg',
    'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg',
];

images.forEach(src => {
    const img = document.createElement('img');
    img.src = src;

    const div = document.createElement('div');
    div.className = 'imgwrapper';
    div.append(img);
    document.querySelector('.sliderwrapper').append(div);
});

let slidestart = 0;

function showSlides() {
    let images = document.getElementsByClassName('imgwrapper');
    
    for (let i = 0; i < images.length; i++) {
        images[i].style.display = 'none';}
    
    slidestart++;
    
    if (slidestart > images.length) {slidestart = 1}
    images[slidestart-1].style.display = 'block';
    setTimeout(showSlides, 3000);
}

showSlides();

