/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн,
//не героїв а планети https://swapi.dev/api/planets, planets, взяти всі 60, обробити в вивести на сторінку//

список справ з https://jsonplaceholder.typicode.com/ 
//виводити які з можливістю редагування при натискані 
*/

const req = async (url) => {
    document.querySelector(".box_loader").classList.add("show")
    const data = await fetch(url);
    return await data.json();
}

const nav = document.querySelector(".nav")
    .addEventListener("click", (e) => {
        if (e.target.dataset.link === "nbu") {
            req("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
                .then((info) => {
                    show(info)
                })
        } else if (e.target.dataset.link === "star") {
            const baseUrl = "https://swapi.dev/api/planets/", allPlanets = [];
            let nextUrl = baseUrl;
        
            const getAllPlanets = async (url) => {
                try {
                    document.querySelector(".box_loader").classList.add("show");
                    const response = await fetch(url), data = await response.json();
                    allPlanets.push(...data.results);
                    nextUrl = data.next;
                    if (nextUrl) {
                        await getAllPlanets(nextUrl);
                    } else {                        
                        const newData = allPlanets.map((planet) => {
                            //console.log(planet.name)
                            return {
                                name: planet.name,
                                climate: planet.climate,
                                population: planet.population
                            };
                        });
                        show(newData);
                        document.querySelector(".box_loader").classList.remove("show");
                        //console.log(newData)
                    }
                } catch (error) {
                    console.error(error);
                    document.querySelector(".box_loader").classList.remove("show");
                }
            };
        
            getAllPlanets(baseUrl);
        } else if (e.target.dataset.link === "todo") {
            req("https://jsonplaceholder.typicode.com/todos")
            .then((info) => {
                show(info)
            })
        } else {

        }
    })

function show(data = []) {
    if (!Array.isArray(data)) return;

    const tbody = document.querySelector("tbody");
  tbody.innerHTML = ""
    const newArr = data.map(({txt, rate, exchangedate, title, completed, name, climate, population }, i) => {
        return {
            id: i + 1,
            name : txt || title || name,
            info1 : rate || completed || climate,
            info2 : exchangedate || population || "тут пусто",
        }
    });

    newArr.forEach(({ name, id, info1, info2 }) => {
        tbody.insertAdjacentHTML("beforeend", `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `)
    })

    document.querySelector(".box_loader").classList.remove("show")
}