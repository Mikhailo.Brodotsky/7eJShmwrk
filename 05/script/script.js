const doc = {
    title: '',
    body: '',
    footer: '',
    date: '',
    app: {
        title: {
            title: '',
        },
        body: {
            body: '',
        },
        footer: {
            footer: '',
        },
        date: {
            date: '',
        }
      },
    dataInAndOut: () => {
        for (prop in doc) {
            if (typeof (doc[prop]) === 'string') {
                doc[prop] = prompt('Set ' + prop + ' document', '');
                document.write(`<p>${prop} of document : ${doc[prop]}`);
            }
          else if (typeof (doc[prop]) === 'object') {
                for (prop in doc.app) {
                    for (prop in doc.app[prop]) {
                        doc.app[prop][prop] = prompt('Set ' + prop + ' application document', '');
                        document.write(`<p>${prop} of document application : ${doc.app[prop][prop]}`);
                    }
                }
            }
        }
    },
}
    
doc.dataInAndOut();
    
console.log(doc)