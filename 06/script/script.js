class Worker {
    constructor() {
        this.name = prompt('Set name', 'Wild');
        this.surname = prompt('Set surname', 'Bill');
        this.rate = prompt('Set rate', 200);
        this.days = prompt('Set days', 2);
    }
    getSalary = () => {
        document.write(`<p>Salary of ${this.name} ${this.surname} is ${this.rate * this.days}`);
    }
}

const test01 = new Worker();
test01.getSalary();
console.log(test01);

class MyString {
    constructor() {
        this.string = prompt('Set string', 'строка для тесту');
    }
    reverse = () => {
        document.write(`<p>Base string: ${this.string}`);
        document.write(`<p>Reversed string: ${this.string.split('').reverse().join('')}`);
    }
    ucFirst = () => {
        document.write(`<p>Base string ${this.string}`);
        document.write(`<p>String with first symbol to uppercase: ${this.string[0].toUpperCase()+this.string.slice(1)}`);
    }
    ucWords = () => {
        document.write(`<p>Base string ${this.string}`);
        document.write(`<p>String with first symbol in every word to uppercase: ${this.string.replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, letter => letter.toUpperCase())}`);
    }    
}

const test02 = new MyString();
test02.reverse();
test02.ucFirst();
test02.ucWords();
console.log(test02)



class Phone {
    constructor () {
        this.number = prompt('Set number', '911');
        this.model = prompt('Set model', 'Iphone');
        this.weight = prompt('Set weight', 222);
    }
    phoneDataOut = () => {
        console.log(this.number);
        console.log(this.model);
        console.log(this.weight);
    }
    receiveCall = () => {
        console.log(`Телефонуе: ${this.number}`);
    }
    getNumber = () => {
        return this.number;
    }   
}

const test03_01 = new Phone();
const test03_02 = new Phone();
const test03_03 = new Phone();
console.log(test03_01);
console.log(test03_02);
console.log(test03_03);
test03_01.phoneDataOut();
test03_01.receiveCall();
console.log(test03_01.getNumber());
test03_02.phoneDataOut();
test03_02.receiveCall();
console.log(test03_02.getNumber());
test03_03.phoneDataOut();
test03_03.receiveCall();
console.log(test03_03.getNumber());


class Driver {
    constructor () {
        this.fullName = prompt('Set full name', 'Sean Connery');
        this.drivingExperience = prompt('Set driving experience', '42');
    }
}

class Engine {
    constructor () {
        this.power = prompt('Set power', '300');
        this.manufacturer = prompt('Set manufacturer', 'Dodge');
    }
}

class Car {
    constructor () {
        this.type = prompt('Set type', 'Neon');
        this.class = prompt('Set class', 'C-Class');        
    }

    Driver = new Driver()

    Engine = new Engine()

    start = () => {
        document.write(`<p>Поїхали`);
    }  
    stop = () => {
        document.write(`<p>Зупиняємося`);
    }
    turnRight = () => {
        document.write(`<p>Поворот праворуч`);
    }       
    turnLeft = () => {
        document.write(`<p>Поворот ліворуч`);
    }
   toString = () => {
        document.write(`<p> ${this.type}, ${this.class}, ${this.Driver.fullName}, ${this.Driver.drivingExperience}, ${this.Engine.power}, ${this.Engine.manufacturer}`);
    }
}

class Lorry extends Car {
        constructor () {
        super();
        this.loadCapacity = prompt('Set load-carrying capacity', '5000');     
    }
}

class SportCar extends Car {
    constructor () {
    super();
    this.maxSpeed = prompt('Set speed limit', '300');     
    }
}

const test04_01 = new Driver();
const test04_02 = new Engine();
const test04_03 = new Car();
const test04_04 = new Lorry();
const test04_05 = new SportCar();
console.log(test04_01);
console.log(test04_02);
console.log(test04_03);
console.log(test04_04);
console.log(test04_05);
test04_03.start();
test04_03.stop();
test04_03.turnRight();
test04_03.turnLeft();
test04_03.toString();
test04_04.toString();
test04_05.toString();


class Animal {
    constructor () {
        this.food = prompt('Set food', 'meat');
        this.location = prompt('Set location', 'jungle');        
    }
    makeNoise  = () => {
        console.log('Така тварина makes some noise');
    }
    eat  = () => {
        console.log('Така тварина їсть');
    }
    sleep  = () => {
        console.log('Така тварина спить');
    }      
}

class Dog extends Animal {
    constructor () {
        super();
        this.tale = prompt('Set tale', 'short tale');            
    }
    makeNoise  = () => {
        console.log('dog barking');
    }
    eat  = () => {
        console.log('dog eat pedigree');
    }
    sleep  = () => {
        console.log('Така тварина спить');
    }      
}

class Cat extends Animal {
    constructor () {
        super();
        this.whiskers = prompt('Set whiskers', 'long whiskers');            
    }
    makeNoise  = () => {
        console.log('cat meows');
    }
    eat  = () => {
        console.log('cat eat whiskas');
    }
    sleep  = () => {
        console.log('Така тварина спить');
    }      
}

class Horse extends Animal {
    constructor () {
        super();
        this.mane = prompt('Set mane', 'long mane');            
    }
    makeNoise  = () => {
        console.log('horse neighs');
    }
    eat  = () => {
        console.log('horse eat oats');
    }
    sleep  = () => {
        console.log('Така тварина спить');
    }      
}

class Veterinarian extends Animal {
    treatAnimal = () => {
        console.log(this.food);
        console.log(this.location);
    }
    main = () => {
        const array = ['dog', 'cat', 'horse'];
        for (const value of array) {
            console.log(`${value} visiting veterinarian`)                    }
    }
}


const test05_01 = new Animal();
const test05_02 = new Dog();
const test05_03 = new Cat()
const test05_04 = new Horse()
const test05_05 = new Veterinarian()

console.log(test05_01);
console.log(test05_02);
console.log(test05_03);
console.log(test05_04);
console.log(test05_05);
test05_01.makeNoise();
test05_02.makeNoise();
test05_03.makeNoise();
test05_04.makeNoise();
test05_02.eat();
test05_03.eat();
test05_04.eat();
test05_05.treatAnimal();
test05_05.main();
